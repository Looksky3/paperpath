﻿using UnityEditor;

namespace Assets.Scripts.AudioManager
{
    [CustomPropertyDrawer(typeof(AudioClipStorage))]
    public class AudioClipStoragePropertyDrawer : SerializableDictionaryStoragePropertyDrawer { }
    [CustomPropertyDrawer(typeof(AudioDictionairy))]
    public class AudioDictionairyPropertyDrawer : SerializableDictionaryPropertyDrawer { }
}
