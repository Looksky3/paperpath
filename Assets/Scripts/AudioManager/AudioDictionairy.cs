﻿using System;
using UnityEngine;

namespace Assets.Scripts.AudioManager
{
    [Serializable]
    public class AudioClipStorage : SerializableDictionary.Storage<AudioClip[]> { }
    [Serializable]
    public class AudioDictionairy : SerializableDictionary<AudioClip, AudioClip[], AudioClipStorage> { }
}

