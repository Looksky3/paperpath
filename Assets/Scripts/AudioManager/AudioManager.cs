﻿using UnityEngine;
using System.Linq;

namespace Assets.Scripts.AudioManager
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioManager : MonoBehaviour
    {
        private AudioSource _audioSource;
        [SerializeField] private AudioDictionairy _audioDictionairy;

        private AudioClip _currentClip;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            SetNewAudioClip(_audioDictionairy.First().Key);
        }

        private void FixedUpdate()
        {
            if(_audioSource.time + Time.deltaTime > _currentClip.length)
            {
                var newClips = _audioDictionairy[_currentClip];
                SetNewAudioClip(newClips[Random.Range(0, newClips.Length)]);
            }
        }

        private void SetNewAudioClip(AudioClip clip)
        {
            _currentClip = clip;
            _audioSource.clip = clip;
            _audioSource.Play();
        }
    }
}
