﻿using UnityEngine;

namespace Assets.Scripts.Game
{
    public interface IGameState
    {
        IGameState Drag(Vector2 startingGridLocation, Vector2 currentGridLocation, bool dragging, GameBoard board);
    }
}
