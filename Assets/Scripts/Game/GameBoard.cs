﻿using Assets.Scripts.Game.Tiles;

namespace Assets.Scripts.Game
{
    public class GameBoard
    {
        public Tile[,] Tiles { get; private set; }

        public GameBoard()
        {
            Tiles = new Tile[Constants.HeightInBlocks, Constants.WidthInBlocks];
        }
    }
}
