﻿using Assets.Scripts.ClickableGrid;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class GameManager : MonoBehaviour
    {
        private IGameState _currentState;
        private readonly GameBoard Board = new GameBoard();

        private void Awake()
        {
            DragChangedEvent.OnDragChanged += DragChangedOnGrid;
            SetDummyBoard();
        }

        private void SetDummyBoard()
        {
            for(int i = 0; i < 7; i++)
            {
                for(int j = 0; j < 7; j++)
                {
                    Board.Tiles[i, j] = new Tiles.Tile() { Type = Tiles.TileType.None };
                }
            }
            Board.Tiles[3, 2] = new Tiles.Tile() { Type = Tiles.TileType.Blank };
            Board.Tiles[3, 3] = new Tiles.Tile() { Type = Tiles.TileType.Blank };
            Board.Tiles[3, 4] = new Tiles.Tile() { Type = Tiles.TileType.Blank };
            Board.Tiles[2, 3] = new Tiles.Tile() { Type = Tiles.TileType.Start };
            Board.Tiles[5, 3] = new Tiles.Tile() { Type = Tiles.TileType.Start };
        }

        private void DragChangedOnGrid(Vector2 startingGridLocation, Vector2 currentGridLocation, bool dragging)
        {
            _currentState = _currentState.Drag(startingGridLocation, currentGridLocation, dragging, Board);
        }
    }
}
