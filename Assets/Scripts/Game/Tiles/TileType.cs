﻿namespace Assets.Scripts.Game.Tiles
{
    public enum TileType
    {
        None,
        Start,
        End,
        DisappearingEnd,
        Blank
    }
}
