﻿using UnityEngine;

namespace Assets.Scripts.ClickableGrid
{
    public class DragChangedEvent
    {
        public delegate void DragChangedEventHandler(Vector2 startingGridLocation, Vector2 currentGridLocation, bool dragging);
        public static DragChangedEventHandler OnDragChanged;

        public static void Call(Vector2 startingGridLocation, Vector2 currentGridLocation, bool dragging)
        {
            if (OnDragChanged != null)
                OnDragChanged(startingGridLocation, currentGridLocation, dragging);
        }
    }
}
