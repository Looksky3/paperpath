﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.Scripts.ClickableGrid
{
    [RequireComponent(typeof(EventTrigger))]
    [RequireComponent(typeof(RectTransform))]
    public class ClickableGridHandler : MonoBehaviour
    {
        private Vector2 _sizePerBlock;
        private Vector2 _minPositionBackground, _maxPositionBackground;
        private Vector2 _startGridPosition, _currentGridPosition;

        private void Start()
        {
            EventTrigger trigger = GetComponent<EventTrigger>();

            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.BeginDrag;
            entry.callback.AddListener((data) => { OnBeginDrag((PointerEventData)data); });
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.Drag;
            entry.callback.AddListener((data) => { OnDrag((PointerEventData)data); });
            trigger.triggers.Add(entry);

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.EndDrag;
            entry.callback.AddListener((data) => { OnEndDrag((PointerEventData)data); });
            trigger.triggers.Add(entry);

            RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
            Vector3 localPositionLeftBottom = new Vector3(rectTransform.offsetMin.x, rectTransform.offsetMin.y, rectTransform.localPosition.z);
            Vector3 localPositionRightTop = new Vector3(rectTransform.offsetMax.x, rectTransform.offsetMax.y, rectTransform.localPosition.z);
            Vector3 worldPositionLeftBottom = transform.TransformPoint(localPositionLeftBottom);
            Vector3 worldPositionRightTop = transform.TransformPoint(localPositionRightTop);
            _minPositionBackground = new Vector2(worldPositionLeftBottom.x, worldPositionLeftBottom.y);
            _maxPositionBackground = new Vector2(worldPositionRightTop.x, worldPositionRightTop.y);

            _sizePerBlock = new Vector2((_maxPositionBackground.x - _minPositionBackground.x) / Constants.WidthInBlocks,
                                        (_maxPositionBackground.y - _minPositionBackground.y) / Constants.HeightInBlocks);
        }

        private void OnBeginDrag(PointerEventData eventData)
        {
            _startGridPosition = GetGridPosition(eventData.position);
        }

        private Vector2 GetGridPosition(Vector2 position)
        {
            int gridPositionX = Mathf.RoundToInt((position.x - _minPositionBackground.x) / _sizePerBlock.x);
            int gridPositionY = Mathf.RoundToInt((position.y - _minPositionBackground.y) / _sizePerBlock.y);
            return new Vector2(gridPositionX, gridPositionY);
        }

        private void OnDrag(PointerEventData eventData)
        {
            if (eventData.position.x > _maxPositionBackground.x || eventData.position.x < _minPositionBackground.x ||
                eventData.position.y > _maxPositionBackground.y || eventData.position.y < _minPositionBackground.y)
                return;

            Vector2 newGridPosition = GetGridPosition(eventData.position);
            if (newGridPosition == _currentGridPosition)
                return;

            _currentGridPosition = newGridPosition;
            DragChangedEvent.Call(_startGridPosition, _currentGridPosition, true);
        }

        private void OnEndDrag(PointerEventData eventData)
        {
            DragChangedEvent.Call(_startGridPosition, _currentGridPosition, false);
        }
    }
}
