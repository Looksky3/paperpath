﻿using System;

namespace Assets.Scripts.SceneManager
{
    [Serializable]
    public class LevelAttributesDictionairy : SerializableDictionary<LevelType, int> { }
}
