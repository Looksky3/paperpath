﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.SceneManager
{
    public static class SceneLoader
    {
        private static bool _initialized = false;

        private static Dictionary<LevelType, SceneRoot> _allSceneRoots = new Dictionary<LevelType, SceneRoot>();
        
        public static void Initialize()
        {
            if (_initialized) return;

            SceneLoaded.OnSceneLoaded += AddSceneRoot;
            _initialized = true;
        }

        private static void AddSceneRoot(SceneRoot sceneRoot)
        {
            if (_allSceneRoots.ContainsKey(sceneRoot.LevelType))
                Debug.LogError("Scene " + sceneRoot.GetType() + " is build twice.");
            else
                _allSceneRoots.Add(sceneRoot.LevelType, sceneRoot);
        }

        public static void LoadScene(int sceneToLoad, LevelType type)
        {
            if (!_allSceneRoots.ContainsKey(type))
            {
                UnityEngine.SceneManagement.SceneManager.LoadScene(sceneToLoad, LoadSceneMode.Additive);
            }
            foreach (SceneRoot root in _allSceneRoots.Values)
            {
                if (root.LevelType == type)
                    ChangeActiveState(root.gameObject, true);
                else
                    ChangeActiveState(root.gameObject, false);
            }
        }

        private static void ChangeActiveState(GameObject gameObject, bool newActiveState)
        {
            if(gameObject.activeSelf != newActiveState)
            {
                gameObject.SetActive(newActiveState);
            }
        }
    }
}
