﻿using UnityEngine;

namespace Assets.Scripts.SceneManager
{
    public class LevelSwitch : MonoBehaviour
    {
        [SerializeField] private LevelAttributesDictionairy _allLevelIds;

        static LevelSwitch()
        {
            SceneLoader.Initialize();
        }

        public void OpenGameScene()
        {
            SceneLoader.LoadScene(_allLevelIds[LevelType.Game], LevelType.Game);
        }

        public void OpenStartScreen()
        {
            SceneLoader.LoadScene(_allLevelIds[LevelType.Start], LevelType.Start);
        }

        public void OpenCreditsScreen()
        {
            SceneLoader.LoadScene(_allLevelIds[LevelType.Credits], LevelType.Credits);
        }

        public void OpenLevelSelectScreen()
        {
            SceneLoader.LoadScene(_allLevelIds[LevelType.LevelSelect], LevelType.LevelSelect);
        }

        public void OpenOptionsScreen()
        {
            SceneLoader.LoadScene(_allLevelIds[LevelType.Options], LevelType.Options);
        }
    }
}
