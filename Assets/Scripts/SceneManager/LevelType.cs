﻿namespace Assets.Scripts.SceneManager
{
    public enum LevelType
    {
        Credits,
        Game,
        LevelSelect,
        Options,
        Start
    }
}
