﻿namespace Assets.Scripts.SceneManager
{
    public class SceneLoaded
    {
        public delegate void SceneLoadedEventHandler(SceneRoot sceneRoot);
        public static SceneLoadedEventHandler OnSceneLoaded;

        public static void Call(SceneRoot sceneRoot)
        {
            if (OnSceneLoaded != null)
                OnSceneLoaded(sceneRoot);
        }
    }
}
