﻿using UnityEditor;

namespace Assets.Scripts.SceneManager
{
    [CustomPropertyDrawer(typeof(LevelAttributesDictionairy))]
    public class LevelAttributesDictionairyPropertyDrawer : SerializableDictionaryPropertyDrawer { }
}
