﻿using UnityEngine;

namespace Assets.Scripts.SceneManager
{
    public class SceneRoot : MonoBehaviour
    {
        [SerializeField] private LevelType _levelType;
        public LevelType LevelType { get { return _levelType; } }

        private void Start()
        {
            SceneLoaded.Call(this);
        }
    }
}
