﻿namespace Assets
{
    public class Constants
    {
        public const int HeightInBlocks = 7;
        public const int WidthInBlocks = 7;
    }
}
