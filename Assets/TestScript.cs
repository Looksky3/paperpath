﻿using Assets.Scripts.ClickableGrid;
using UnityEngine;
using UnityEngine.UI;

namespace Assets
{
    public class TestScript : MonoBehaviour
    {
        [SerializeField]private Text MyText;

        private void Start()
        {
            DragChangedEvent.OnDragChanged += DragChanged;
        }

        private void DragChanged(Vector2 startingGridLocation, Vector2 currentGridLocation, bool dragging)
        {
            MyText.text = "X: " + currentGridLocation.x + " Y: " + currentGridLocation.y;
        }
    }
}
